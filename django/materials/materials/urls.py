from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'materials.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^auth/', include('materials.apps.loginsys.urls', namespace='loginsys')),
    url(r'^$', include('materials.apps.core.urls', namespace='index')),
)

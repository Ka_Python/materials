from django.conf.urls import patterns, url


urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'materials.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^$', 'materials.apps.core.views.index', name='index'),
)
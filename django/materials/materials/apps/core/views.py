from django.shortcuts import render_to_response
from django.core.context_processors import csrf
from django.contrib import auth

def index(request):
    value = {}
    value.update(csrf(request))
    value['user'] = auth.get_user(request)
    return render_to_response('index.html', value)

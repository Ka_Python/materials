from django.shortcuts import render_to_response
from django.core.context_processors import csrf
from django.contrib import auth
from django.contrib.auth.forms import User
from materials.apps.loginsys.forms import userform, userlightform
from django.contrib.auth.forms import UserCreationForm

def register(request):
    value = {}
    value.update(csrf(request))
    value['user'] = auth.get_user(request)
    value['form'] = userlightform
    if request.POST:
        new_user = userlightform(request.POST)
        if new_user.is_valid():
            username = request.POST['username']
            #email = request.POST['email']
            password1 = request.POST['password1']
            password2 = request.POST['password2']
            user = User
            email = user.objects.normalize_email(request.POST['email'])

    return render_to_response('register.html', value)

def login(request):
    pass

def logout(request):
    pass
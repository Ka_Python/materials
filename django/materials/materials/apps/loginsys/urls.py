from django.conf.urls import patterns, url

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'materials.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^register/$', 'materials.apps.loginsys.views.register', name='register'),
    url(r'^login/$', 'materials.apps.loginsys.views.login', name='login'),
    url(r'^logout/$', 'materials.apps.loginsys.views.logout', name='logout'),
)


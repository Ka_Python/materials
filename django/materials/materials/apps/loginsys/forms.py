from django.forms import ModelForm
from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm

class userform(UserCreationForm):
    email = forms.EmailField(max_length=30, label='электронная почта')

    class Meta:
        model = User
        fields = ("username", "email",)

    def save(self, commit=True):
        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])

        if commit:
            user.save()
        return user

class userlightform(forms.Form):
    username = forms.CharField(max_length=50, label='Имя пользователя')
    email = forms.EmailField(label='Почта')
    password1 = forms.CharField(max_length=15, label='Пароль', widget=forms.PasswordInput, help_text='Введите пороль')
    password2 = forms.CharField(max_length=15, label='Подтверждение пароля', widget=forms.PasswordInput, help_text='Подтверждение пароля')